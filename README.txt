TOUCHPAD OR TRACKBALL REQUIRED FOR PLAYING

~TOWER DEFENSE~

::Intro::
The princesses are loose!
Bring them back while defending your tower before time runs out!

::Controls::
SPACE:	fly (while perched), capture (while flying), drop (after capturing)
SWIPE: shoot fireball (while perched), turn (while flying)

::Goal::
-You're lust for damsels in distress only ends when four princesses are in each tower.
-Do so before time runs out
-Careful, there plenty of knights, mages, slimes and skeletons out there to stop your cause!
-Merchants drop loot for a temporary power boost.

::Tips::
Capturing merchants gives longer lasting powerups

::Credits::
Anton Bobkov  (programming)
Peter Lu (art, sound)
Alex Rickett (music)