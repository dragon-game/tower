#include "noisyfly.h"


int main(int argc, char *argv[])
{
	///Initialize the SDL library
	SDL_Init(SDL_INIT_EVERYTHING);
	SDL_Surface* screen = SDL_SetVideoMode( 1, 1, 32, SDL_SWSURFACE | SDL_RESIZABLE); 

	//initialize mixer
	Mix_OpenAudio( 22050, MIX_DEFAULT_FORMAT, 2, 4096 );

	//play a sound
	SoundManager sm;
	int n = sm.loadSound("laser.wav");
	std::cout << n << std::endl;
	sm.playSound(n);

	SDL_Delay(2000);


	return 0;
}