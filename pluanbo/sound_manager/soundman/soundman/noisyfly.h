#ifndef NOISYFLY
#define NOISYFLY
#ifdef WIN32
#pragma comment(lib, "SDL.lib")
#pragma comment(lib, "SDLmain.lib")
#pragma comment(lib, "SDL_mixer.lib")
#endif

#include "SDL.h"
#include "SDL_mixer.h"
#include <string>
#include <map>
#include <iostream>

struct SoundManager
{
	std::map<std::string, Mix_Chunk *> mChunks;
	std::map<int, std::string> mIds;
	int counter;
	SoundManager()
	{
		counter = 0;
	}
	~SoundManager()
	{
		for(std::map<std::string, Mix_Chunk *>::iterator it = mChunks.begin(); it != mChunks.end(); it++)
			Mix_FreeChunk(it->second);
	}

	int loadSound(std::string filename)
	{
		Mix_Chunk * m = Mix_LoadWAV(filename.c_str());
		if(!m)
			return -1;
		counter++;
		mChunks[filename] = m;
		mIds[counter] = filename;
		return counter;
	}

	void playSound(int id)
	{
		if(mIds.find(id) != mIds.end())
		{
			Mix_Volume(0,999999);
			std::cout << "playing sound " << Mix_PlayChannel( -1, getChunk(id), 0 ) << std::endl;
			std::cout << Mix_Playing(0);
		}
	}

	Mix_Chunk * getChunk(int id)
	{
		return mChunks[mIds[id]];
	}

};
#endif